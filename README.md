# README

Rails 7 new project generated with `rails new -c bootstrap -d postgresql app-bootstrap`.

This app was generated from a container using the Dockerfile of this project.

## Instructions to reproduce

1. build the docker image
```sh
docker build -t app-bootstrap .
```
2. go the dir where the project should be generated (for me it is `~/dev`)
```sh
cd ~/dev
docker run --rm -it -v $(pwd):/home/dev app-bootstrap bash
$ cd /home/dev
$ rails new -c bootstrap -d postgresql app-bootstrap
```
3. to use docker, add the `Dockfile`, `docker-compose.yml` & `entrypoint.sh` and edit the `Procfile.dev`

4. to start the project using docker:
```sh
docker compose up
```
First time, you need to create the database:
```sh
docker exec -it $(docker ps | grep rails | awk '{ print $1 }') rails db:create
```
→ http://localhost:3000
