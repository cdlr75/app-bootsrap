FROM ruby:3.2.2

RUN apt-get update -qq && apt-get install -y postgresql-client
# install node & npm using nvm and get yarn
# nvm env vars
ENV NODE_VERSION v18.16.0
ENV NVM_DIR /usr/local/nvm
RUN mkdir -p $NVM_DIR
RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.3/install.sh | bash
RUN /bin/bash -c "source $NVM_DIR/nvm.sh && nvm install $NODE_VERSION"
# add node and npm to the PATH
RUN ls $NVM_DIR/versions/
ENV NODE_PATH $NVM_DIR/versions/node/$NODE_VERSION/bin
ENV PATH $NODE_PATH:$PATH
RUN npm -v
RUN node -v
# install yarn
RUN npm install -g yarn

WORKDIR /app

COPY Gemfile Gemfile.lock package.json ./
# Install dependencies
RUN bundle install
RUN yarn install

COPY . .

# Add a script to be executed every time the container starts.
COPY entrypoint.sh /usr/bin/
RUN chmod +x /usr/bin/entrypoint.sh
ENTRYPOINT ["entrypoint.sh"]
EXPOSE 3000

# Configure the main process to run when running the image
CMD ["rails", "server", "-b", "0.0.0.0"]
